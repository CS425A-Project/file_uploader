import sys
import os
import datanodes_monitor.datanodes_monitor as dm
import socket
import json
import ftplib

chunk_size = 1024*1024	#Bytes

def get_ipv4_addr(host, port):
    addr = socket.getaddrinfo(host, port, socket.AF_INET, 0, socket.SOL_TCP)
    if len(addr) == 0:
    	raise Exception("There is no IPv4 address configured for host: "+host)
    return addr[0][-1]

def send_data(data, host, port):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(get_ipv4_addr(host, port=port))

	s.send(data.encode())
	response = s.recv(1024)
	if response.decode() != 'OK':
		print('Error: send_data encountered error.')
		return False
	s.close()
	return True

def ftp_upload(datanode_details,file_temp):
	try:
		ftp = ftplib.FTP()
		ftp.connect(datanode_details['ip'])
		ftp.login('botnet', 'botnet')
		ftp.storbinary('STOR ' + datanode_details['filename'], open(file_temp, 'rb'))
		ftp.close()
		return True
	except:
		print('Error: error while performing FTP.')
		return False

def revert_upload(datanode,filename):
	try:
		data = json.JSONEncoder().encode(
				{
				'val': datanode['val'],
				'ip': datanode['ip'],
				'port': datanode['port']
				}
				)
		while data['val'] != NULL:
			encoded_data = json.JSONEncoder().encode(
					{
					'type': 'delete',
					'filename': data['val']
					}
					)
			ftp = ftplib.FTP()
			ftp.connect(data['ip'],data['port'])
			ftp.login('botnet', 'botnet')
			ftp.delete(data['val'])
			data = send_data(encoded_data, data['ip'], data['port'])
		return False
	except:
		print('Error: error while reverting the upload')
		return False

def upload_file(abs_file_path):
	f = open(abs_file_path, 'rb+')
	file_temp = open('/tmp/botnet_temp','wb+')
	chunk=f.read(chunk_size)
	file_temp.write(chunk)
	file_temp.close()

	multi_datanodes = dm.get_max_three_datanodes()
	initial_node = multi_datanodes
	for ip in multi_datanodes['ip']:
		datanode_to_upload = multi_datanodes.copy()
		datanode_to_upload['ip'] = ip

		if not ftp_upload(datanode_to_upload, '/tmp/botnet_temp'):
			print('Error: FTP failed.')
			return revert_upload(initial_node,abs_file_path)
	
	chunk=f.read(chunk_size)

	while chunk != bytes():
		file_temp = open('/tmp/botnet_temp','wb+')
		file_temp.write(chunk)
		file_temp.close()
		prev_datanode = multi_datanodes

		multi_datanodes = dm.get_max_three_datanodes()
		for ip in multi_datanodes['ip']:

			datanode_to_upload = multi_datanodes.copy()
			datanode_to_upload['ip'] = ip
			if not ftp_upload(datanode_to_upload,'/tmp/botnet_temp'):
				print('Error: FTP failed.')
				return revert_upload(initial_node,abs_file_path)
			encoded_data = json.JSONEncoder().encode(
				{
				'type': 'insert',
				'prev_filename':  prev_datanode['filename'],
				'curr_filename': datanode_to_upload['filename'],
				'ip': datanode_to_upload['ip'],
				'port':datanode_to_upload['port']
				}
				)
			print(encoded_data)
			for ip_to_send in prev_datanode['ip']:
				resp = send_data(encoded_data, ip_to_send, prev_datanode['port'])
				if not resp:
					print('Error: send_data failed.')
					return revert_upload(initial_node,abs_file_path)
		chunk=f.read(chunk_size)
	file_temp.close()
	f.close()
	print(initial_node)
	return initial_node

def init_dm(db_file_path):
	dm.init_monitor(db_file_path)
